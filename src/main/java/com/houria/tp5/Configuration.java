package com.houria.tp5;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.Toast;

public class Configuration extends Fragment {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    TextInputEditText urlInput;
    CheckBox grp_td1;
    CheckBox grp_td2;
    CheckBox grp_td3;
    CheckBox grp_td4;
    CheckBox grp_td5;
    CheckBox grp_td6;
    CheckBox grp_td7;
    CheckBox grp_td8;
    CheckBox grp_td9;
    Button save;
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public Configuration() {
        // Required empty public constructor
    }

    public static Configuration newInstance(String param1, String param2) {
        Configuration fragment = new Configuration();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_configuration, container, false);
        urlInput = view.findViewById(R.id.url_edt);
        grp_td1 = view.findViewById(R.id.gr1);
        grp_td2 = view.findViewById(R.id.gr2);
        grp_td3 = view.findViewById(R.id.gr3);
        grp_td4 = view.findViewById(R.id.gr4);
        grp_td5 = view.findViewById(R.id.gr5);
        grp_td6 = view.findViewById(R.id.gr6);
        grp_td7 = view.findViewById(R.id.gr7);
        grp_td8 = view.findViewById(R.id.gr8);
        grp_td9= view.findViewById(R.id.gr9);

//        getGrpsSharedPrefs();   *********** mahmoud
        save = (Button) view.findViewById(R.id.save_pref);
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences.Editor editor = getContext().getSharedPreferences("grp_td", MainActivity.MODE_PRIVATE).edit();


                //save configuration action
                boolean td1_selected = grp_td1.isChecked();
                boolean td2_selected = grp_td2.isChecked();
                boolean td3_selected = grp_td3.isChecked();
                boolean td4_selected = grp_td4.isChecked();
                boolean td5_selected = grp_td5.isChecked();
                boolean td6_selected = grp_td6.isChecked();
                boolean td7_selected = grp_td7.isChecked();
                boolean td8_selected = grp_td8.isChecked();
                boolean td9_selected = grp_td9.isChecked();


                if(td1_selected){
                    editor.putString("td1_selected", "L3info_td1");
                } else editor.putString("td1_selected", "Word_does_not_exist_in_data");


                if(td2_selected){
                    editor.putString("td2_selected", "L3info_td2");
                }else editor.putString("td2_selected", "Word_does_not_exist_in_data");

                if(td3_selected){
                    editor.putString("td3_selected", "L3info_td3");
                }else editor.putString("td3_selected", "Word_does_not_exist_in_data");

                if(td4_selected){
                    editor.putString("td4_selected", "L3info_td4");
                }else editor.putString("td4_selected", "Word_does_not_exist_in_data");

                if(td5_selected){
                    //td5 alt
                    editor.putString("td5_selected", "L3info_td5 alt");
                }else editor.putString("td5_selected", "Word_does_not_exist_in_data");

                if(td6_selected){
                    //td6 alt
                    editor.putString("td6_selected", "L3info_td6 alt");
                }else editor.putString("td6_selected", "Word_does_not_exist_in_data");

                if(td7_selected){
                    //projet programmation
                    editor.putString("td7_selected", "ue projet programmation 2");
                }else editor.putString("td7_selected", "Word_does_not_exist_in_data");

                if(td8_selected){
                    //projet robot
                    editor.putString("td8_selected", "ue projet robot2");
                }else editor.putString("td8_selected", "Word_does_not_exist_in_data");

                if(td9_selected){
                    //projet syr
                    editor.putString("td9_selected", "ue projet syr 2");
                }else editor.putString("td9_selected", "Word_does_not_exist_in_data");

                editor.putString("url_edt",urlInput.getText().toString());
                editor.apply();

                CharSequence text = "Les modifications ont bien été enregistrées !";
                int duration = Toast.LENGTH_SHORT;
                Toast toast = Toast.makeText(getContext(), text, duration);
                toast.show();

            }
        });

        return view;
    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof Configuration.OnFragmentInteractionListener) {
            mListener = (Configuration.OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }



    }
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }
    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public void getGrpsSharedPrefs(){
        SharedPreferences prefs = getContext().getSharedPreferences("grp_td", MainActivity.MODE_PRIVATE);

        String td1 = prefs.getString("td1_selected", null);

        String td2 = prefs.getString("td2_selected", null);

        String td3 = prefs.getString("td3_selected", null);

        String td4 = prefs.getString("td4_selected", null);

        String td5 = prefs.getString("td5_selected", null);

        String td6 = prefs.getString("td6_selected", null);

        String td7 = prefs.getString("td7_selected", null);

        String td8 = prefs.getString("td8_selected", null);

        String td9 = prefs.getString("td9_selected", null);

        if(!td1.equals("Word_does_not_exist_in_data"))
          grp_td1.setChecked(true);
        if(!td2.equals("Word_does_not_exist_in_data"))
            grp_td2.setChecked(true);
        if(!td3.equals("Word_does_not_exist_in_data"))
            grp_td3.setChecked(true);
        if(!td4.equals("Word_does_not_exist_in_data"))
            grp_td4.setChecked(true);
        if(!td5.equals("Word_does_not_exist_in_data"))
            grp_td5.setChecked(true);
        if(!td6.equals("Word_does_not_exist_in_data"))
            grp_td6.setChecked(true);
        if(!td7.equals("Word_does_not_exist_in_data"))
            grp_td7.setChecked(true);
        if(!td8.equals("Word_does_not_exist_in_data"))
            grp_td8.setChecked(true);
        if(!td6.equals("Word_does_not_exist_in_data"))
            grp_td9.setChecked(true);

        String url_saved = prefs.getString("url_edt", "Default");
        urlInput.setText(url_saved);
    }


}
